<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_cloudflare"></a> [cloudflare](#requirement\_cloudflare) | ~> 4.0.0 |
| <a name="requirement_google"></a> [google](#requirement\_google) | ~> 4.59.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_cloudflare"></a> [cloudflare](#provider\_cloudflare) | ~> 4.0.0 |
| <a name="provider_google"></a> [google](#provider\_google) | ~> 4.59.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_name"></a> [name](#module\_name) | git::https://gitlab.com/terraform-modules1582413/name.git | n/a |

## Resources

| Name | Type |
|------|------|
| [cloudflare_record.gke](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/record) | resource |
| [cloudflare_record.gke_outside_dns](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/resources/record) | resource |
| [google_compute_address.gke_outside_public_ip](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_address) | resource |
| [google_compute_firewall.block_all](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_firewall.master_to_nodes](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_firewall.nodes_to_nodes](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_firewall.pods_to_monitoring_nodes](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_firewall.pods_to_nodes](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_global_address.gke_private_ip_address](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_global_address) | resource |
| [google_compute_network.gke](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_network) | resource |
| [google_compute_router.gke](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_router) | resource |
| [google_compute_router_nat.gke](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_router_nat) | resource |
| [google_compute_shared_vpc_host_project.gke_shared_host](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_shared_vpc_host_project) | resource |
| [google_compute_shared_vpc_service_project.gke_project](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_shared_vpc_service_project) | resource |
| [google_compute_subnetwork.gke](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_subnetwork) | resource |
| [google_compute_subnetwork_iam_binding.gke_shared_host](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_subnetwork_iam_binding) | resource |
| [google_container_cluster.main](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/container_cluster) | resource |
| [google_container_node_pool.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/container_node_pool) | resource |
| [google_project_iam_member.gke_sa_roles](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_project_iam_member.gke_shared_network_roles_agent_user](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_project_iam_member.gke_shared_network_roles_fw_admin](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_service_account.gke](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account) | resource |
| [google_service_networking_connection.gke_private_vpc_connection](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_networking_connection) | resource |
| [cloudflare_zone.main](https://registry.terraform.io/providers/cloudflare/cloudflare/latest/docs/data-sources/zone) | data source |
| [google_project.main](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/project) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cf_base_domain"></a> [cf\_base\_domain](#input\_cf\_base\_domain) | n/a | `string` | n/a | yes |
| <a name="input_cf_gke_dns_record_enabled"></a> [cf\_gke\_dns\_record\_enabled](#input\_cf\_gke\_dns\_record\_enabled) | n/a | `bool` | `true` | no |
| <a name="input_cf_gke_outside_dns_record_enabled"></a> [cf\_gke\_outside\_dns\_record\_enabled](#input\_cf\_gke\_outside\_dns\_record\_enabled) | n/a | `bool` | `true` | no |
| <a name="input_gcp_project_id"></a> [gcp\_project\_id](#input\_gcp\_project\_id) | n/a | `string` | n/a | yes |
| <a name="input_gcp_region"></a> [gcp\_region](#input\_gcp\_region) | n/a | `string` | n/a | yes |
| <a name="input_gcp_shared_networks_enabled"></a> [gcp\_shared\_networks\_enabled](#input\_gcp\_shared\_networks\_enabled) | n/a | `bool` | `false` | no |
| <a name="input_gcp_shared_networks_host_project_id"></a> [gcp\_shared\_networks\_host\_project\_id](#input\_gcp\_shared\_networks\_host\_project\_id) | n/a | `string` | `""` | no |
| <a name="input_gke_authorized_networks"></a> [gke\_authorized\_networks](#input\_gke\_authorized\_networks) | n/a | <pre>list(object({<br>    cidr_block   = string<br>    display_name = string<br>  }))</pre> | n/a | yes |
| <a name="input_gke_custom_pods"></a> [gke\_custom\_pods](#input\_gke\_custom\_pods) | n/a | <pre>object({<br>    range_name = string<br>    ip_range   = string<br>  })</pre> | <pre>{<br>  "ip_range": "10.40.0.0/16",<br>  "range_name": "custom-pods"<br>}</pre> | no |
| <a name="input_gke_custom_pods_enabled"></a> [gke\_custom\_pods\_enabled](#input\_gke\_custom\_pods\_enabled) | n/a | `bool` | `false` | no |
| <a name="input_gke_default_node_pool_disk_size_gb"></a> [gke\_default\_node\_pool\_disk\_size\_gb](#input\_gke\_default\_node\_pool\_disk\_size\_gb) | n/a | `number` | `24` | no |
| <a name="input_gke_default_node_pool_disk_type"></a> [gke\_default\_node\_pool\_disk\_type](#input\_gke\_default\_node\_pool\_disk\_type) | n/a | `string` | `"pd-standard"` | no |
| <a name="input_gke_default_node_pool_image_type"></a> [gke\_default\_node\_pool\_image\_type](#input\_gke\_default\_node\_pool\_image\_type) | n/a | `string` | `"UBUNTU_CONTAINERD"` | no |
| <a name="input_gke_default_node_pool_initial_node_count"></a> [gke\_default\_node\_pool\_initial\_node\_count](#input\_gke\_default\_node\_pool\_initial\_node\_count) | n/a | `number` | `1` | no |
| <a name="input_gke_default_node_pool_labels"></a> [gke\_default\_node\_pool\_labels](#input\_gke\_default\_node\_pool\_labels) | n/a | `map(string)` | <pre>{<br>  "name": "default"<br>}</pre> | no |
| <a name="input_gke_default_node_pool_machine_type"></a> [gke\_default\_node\_pool\_machine\_type](#input\_gke\_default\_node\_pool\_machine\_type) | n/a | `string` | `"e2-small"` | no |
| <a name="input_gke_default_node_pool_max_node_count"></a> [gke\_default\_node\_pool\_max\_node\_count](#input\_gke\_default\_node\_pool\_max\_node\_count) | n/a | `number` | `1` | no |
| <a name="input_gke_default_node_pool_min_node_count"></a> [gke\_default\_node\_pool\_min\_node\_count](#input\_gke\_default\_node\_pool\_min\_node\_count) | n/a | `number` | `1` | no |
| <a name="input_gke_default_node_pool_name"></a> [gke\_default\_node\_pool\_name](#input\_gke\_default\_node\_pool\_name) | n/a | `string` | `"default"` | no |
| <a name="input_gke_default_node_pool_preemptible"></a> [gke\_default\_node\_pool\_preemptible](#input\_gke\_default\_node\_pool\_preemptible) | n/a | `bool` | `false` | no |
| <a name="input_gke_default_node_pool_spot"></a> [gke\_default\_node\_pool\_spot](#input\_gke\_default\_node\_pool\_spot) | n/a | `bool` | `false` | no |
| <a name="input_gke_default_pods"></a> [gke\_default\_pods](#input\_gke\_default\_pods) | n/a | <pre>object({<br>    range_name = string<br>    ip_range   = string<br>  })</pre> | <pre>{<br>  "ip_range": "10.30.0.0/16",<br>  "range_name": "pods"<br>}</pre> | no |
| <a name="input_gke_description"></a> [gke\_description](#input\_gke\_description) | n/a | `string` | n/a | yes |
| <a name="input_gke_location"></a> [gke\_location](#input\_gke\_location) | n/a | `string` | n/a | yes |
| <a name="input_gke_maintenance_policy"></a> [gke\_maintenance\_policy](#input\_gke\_maintenance\_policy) | n/a | <pre>object({<br>    start_time = string<br>    end_time   = string<br>    recurrence = string<br>  })</pre> | <pre>{<br>  "end_time": "2022-05-01T04:00:00Z",<br>  "recurrence": "FREQ=WEEKLY;BYDAY=TU,WE,TH;INTERVAL=1",<br>  "start_time": "2022-05-01T00:00:00Z"<br>}</pre> | no |
| <a name="input_gke_master_ipv4_cidr_block"></a> [gke\_master\_ipv4\_cidr\_block](#input\_gke\_master\_ipv4\_cidr\_block) | n/a | `string` | `"172.16.0.0/28"` | no |
| <a name="input_gke_nodes_ip_range"></a> [gke\_nodes\_ip\_range](#input\_gke\_nodes\_ip\_range) | n/a | `string` | `"10.10.0.0/16"` | no |
| <a name="input_gke_private_vpc_connection_enabled"></a> [gke\_private\_vpc\_connection\_enabled](#input\_gke\_private\_vpc\_connection\_enabled) | n/a | `bool` | `true` | no |
| <a name="input_gke_release_channel"></a> [gke\_release\_channel](#input\_gke\_release\_channel) | n/a | `string` | `"STABLE"` | no |
| <a name="input_gke_security_group_domain"></a> [gke\_security\_group\_domain](#input\_gke\_security\_group\_domain) | n/a | `string` | `""` | no |
| <a name="input_gke_security_group_rbac_enabled"></a> [gke\_security\_group\_rbac\_enabled](#input\_gke\_security\_group\_rbac\_enabled) | n/a | `bool` | `false` | no |
| <a name="input_gke_services"></a> [gke\_services](#input\_gke\_services) | n/a | <pre>object({<br>    range_name = string<br>    ip_range   = string<br>  })</pre> | <pre>{<br>  "ip_range": "10.20.0.0/16",<br>  "range_name": "services"<br>}</pre> | no |
| <a name="input_gke_vertical_pod_autoscaling_enabled"></a> [gke\_vertical\_pod\_autoscaling\_enabled](#input\_gke\_vertical\_pod\_autoscaling\_enabled) | n/a | `bool` | `false` | no |
| <a name="input_gke_workload_identity_enabled"></a> [gke\_workload\_identity\_enabled](#input\_gke\_workload\_identity\_enabled) | n/a | `bool` | `false` | no |
| <a name="input_project_environment"></a> [project\_environment](#input\_project\_environment) | n/a | `string` | n/a | yes |
| <a name="input_project_name"></a> [project\_name](#input\_project\_name) | n/a | `string` | n/a | yes |
| <a name="input_project_prefix"></a> [project\_prefix](#input\_project\_prefix) | n/a | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cluster_basic_info"></a> [cluster\_basic\_info](#output\_cluster\_basic\_info) | n/a |
| <a name="output_cluster_ca_cert"></a> [cluster\_ca\_cert](#output\_cluster\_ca\_cert) | n/a |
| <a name="output_cluster_endpoint"></a> [cluster\_endpoint](#output\_cluster\_endpoint) | n/a |
| <a name="output_cluster_full_endpoint"></a> [cluster\_full\_endpoint](#output\_cluster\_full\_endpoint) | n/a |
| <a name="output_cluster_ipv4_cidr"></a> [cluster\_ipv4\_cidr](#output\_cluster\_ipv4\_cidr) | n/a |
| <a name="output_cluster_name"></a> [cluster\_name](#output\_cluster\_name) | n/a |
| <a name="output_global_network_tag"></a> [global\_network\_tag](#output\_global\_network\_tag) | n/a |
| <a name="output_network_id"></a> [network\_id](#output\_network\_id) | n/a |
| <a name="output_network_name"></a> [network\_name](#output\_network\_name) | n/a |
| <a name="output_network_project_id"></a> [network\_project\_id](#output\_network\_project\_id) | n/a |
| <a name="output_network_self_link"></a> [network\_self\_link](#output\_network\_self\_link) | n/a |
| <a name="output_outside_public_ip"></a> [outside\_public\_ip](#output\_outside\_public\_ip) | n/a |
| <a name="output_pods_ip_ranges"></a> [pods\_ip\_ranges](#output\_pods\_ip\_ranges) | n/a |
| <a name="output_pods_range_name"></a> [pods\_range\_name](#output\_pods\_range\_name) | n/a |
| <a name="output_sa_email"></a> [sa\_email](#output\_sa\_email) | n/a |
<!-- END_TF_DOCS -->