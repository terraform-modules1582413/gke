locals {
  roles = [
    "roles/logging.logWriter",
    "roles/monitoring.metricWriter",
    "roles/monitoring.viewer",
    "roles/stackdriver.resourceMetadata.writer",
    "roles/artifactregistry.reader"
  ]
}

resource "google_service_account" "gke" {
  project      = var.gcp_project_id
  account_id   = module.name.short
  display_name = "Service account for ${module.name.short}"

  depends_on = [
    google_compute_shared_vpc_service_project.gke_project,
    google_project_iam_member.gke_shared_network_roles_agent_user,
    google_project_iam_member.gke_shared_network_roles_fw_admin
  ]
}

resource "google_project_iam_member" "gke_sa_roles" {
  for_each = toset(local.roles)

  role    = each.value
  member  = "serviceAccount:${google_service_account.gke.email}"
  project = var.gcp_project_id
}

resource "google_project_iam_member" "gke_shared_network_roles_fw_admin" {
  count = var.gcp_shared_networks_enabled ? 1 : 0

  role    = "roles/compute.securityAdmin"
  member  = "serviceAccount:service-${data.google_project.main.number}@container-engine-robot.iam.gserviceaccount.com"
  project = var.gcp_shared_networks_host_project_id

  depends_on = [google_compute_shared_vpc_service_project.gke_project]
}

resource "google_project_iam_member" "gke_shared_network_roles_agent_user" {
  count = var.gcp_shared_networks_enabled ? 1 : 0

  role    = "roles/container.hostServiceAgentUser"
  member  = "serviceAccount:service-${data.google_project.main.number}@container-engine-robot.iam.gserviceaccount.com"
  project = var.gcp_shared_networks_host_project_id

  depends_on = [google_compute_shared_vpc_service_project.gke_project]
}
