locals {
  secondary_ip_ranges = flatten([
    [
      {
        range_name    = var.gke_services.range_name
        ip_cidr_range = var.gke_services.ip_range
      },
      {
        range_name    = var.gke_default_pods.range_name
        ip_cidr_range = var.gke_default_pods.ip_range
      }
    ],
    var.gke_custom_pods_enabled ? [
      {
        range_name    = var.gke_custom_pods.range_name
        ip_cidr_range = var.gke_custom_pods.ip_range
      }
    ] : []
  ])
}

resource "google_compute_shared_vpc_host_project" "gke_shared_host" {
  count   = var.gcp_shared_networks_enabled ? 1 : 0
  project = var.gcp_shared_networks_host_project_id
}

resource "google_compute_shared_vpc_service_project" "gke_project" {
  count           = var.gcp_shared_networks_enabled ? 1 : 0
  host_project    = google_compute_shared_vpc_host_project.gke_shared_host[0].project
  service_project = var.gcp_project_id

  depends_on = [google_compute_shared_vpc_host_project.gke_shared_host]
}

resource "google_compute_network" "gke" {
  project                 = var.gcp_shared_networks_enabled ? var.gcp_shared_networks_host_project_id : var.gcp_project_id
  name                    = "${module.name.short}-vpc-${module.name.appendix}"
  auto_create_subnetworks = false

  depends_on = [google_compute_shared_vpc_service_project.gke_project]
}

resource "google_compute_subnetwork" "gke" {
  project                  = var.gcp_shared_networks_enabled ? var.gcp_shared_networks_host_project_id : var.gcp_project_id
  name                     = "${module.name.short}-subnet-${module.name.appendix}"
  region                   = var.gcp_region
  network                  = google_compute_network.gke.id
  ip_cidr_range            = var.gke_nodes_ip_range
  secondary_ip_range       = local.secondary_ip_ranges
  private_ip_google_access = true
}

resource "google_compute_subnetwork_iam_binding" "gke_shared_host" {
  count      = var.gcp_shared_networks_enabled ? 1 : 0
  project    = google_compute_subnetwork.gke.project
  region     = google_compute_subnetwork.gke.region
  subnetwork = google_compute_subnetwork.gke.name
  role       = "roles/compute.networkUser"
  members = [
    "serviceAccount:${data.google_project.main.number}@cloudservices.gserviceaccount.com",
    "serviceAccount:service-${data.google_project.main.number}@container-engine-robot.iam.gserviceaccount.com"
  ]
}

resource "google_compute_global_address" "gke_private_ip_address" {
  count         = var.gke_private_vpc_connection_enabled ? 1 : 0
  project       = var.gcp_shared_networks_enabled ? var.gcp_shared_networks_host_project_id : var.gcp_project_id
  name          = "${module.name.short}-private-svc-ips-${module.name.appendix}"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = google_compute_network.gke.id
}

resource "google_service_networking_connection" "gke_private_vpc_connection" {
  count                   = var.gke_private_vpc_connection_enabled ? 1 : 0
  network                 = google_compute_network.gke.id
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.gke_private_ip_address[0].name]
}

resource "google_compute_router" "gke" {
  project = var.gcp_shared_networks_enabled ? var.gcp_shared_networks_host_project_id : var.gcp_project_id
  name    = "${module.name.short}-router-${module.name.appendix}"
  region  = var.gcp_region
  network = google_compute_network.gke.name
}

resource "google_compute_address" "gke_outside_public_ip" {
  project      = var.gcp_shared_networks_enabled ? var.gcp_shared_networks_host_project_id : var.gcp_project_id
  name         = "${module.name.short}-outside-${module.name.appendix}"
  address_type = "EXTERNAL"
  region       = var.gcp_region
}

resource "cloudflare_record" "gke_outside_dns" {
  count   = var.cf_gke_outside_dns_record_enabled ? 1 : 0
  zone_id = data.cloudflare_zone.main.zone_id
  name    = "${module.name.domain_friendly}-outside"
  value   = google_compute_address.gke_outside_public_ip.address
  type    = "A"
  ttl     = "300"
  proxied = false
}

resource "google_compute_router_nat" "gke" {
  project                            = var.gcp_shared_networks_enabled ? var.gcp_shared_networks_host_project_id : var.gcp_project_id
  name                               = "${module.name.short}-nat-${module.name.appendix}"
  router                             = google_compute_router.gke.name
  region                             = var.gcp_region
  source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"

  nat_ip_allocate_option = "MANUAL_ONLY"
  nat_ips                = [google_compute_address.gke_outside_public_ip.self_link]

  subnetwork {
    name                    = google_compute_subnetwork.gke.self_link
    source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
  }

  enable_dynamic_port_allocation      = true
  enable_endpoint_independent_mapping = false
  min_ports_per_vm                    = 256

  log_config {
    enable = true
    filter = "ERRORS_ONLY"
  }
}
