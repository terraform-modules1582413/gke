data "google_project" "main" {
  project_id = var.gcp_project_id
}

data "cloudflare_zone" "main" {
  name = var.cf_base_domain
}
