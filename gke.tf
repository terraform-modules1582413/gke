locals {
  default_nodes_network_tag = "${module.name.short}-all-nodes-${module.name.appendix}"
}

resource "google_container_cluster" "main" {
  project     = var.gcp_project_id
  name        = "${module.name.short}-${module.name.appendix}"
  location    = var.gke_location
  description = var.gke_description

  resource_labels = {
    "project"     = var.project_name
    "environment" = var.project_environment
  }

  initial_node_count       = 1
  remove_default_node_pool = true

  networking_mode = "VPC_NATIVE"

  private_cluster_config {
    enable_private_endpoint = false
    enable_private_nodes    = true
    master_ipv4_cidr_block  = var.gke_master_ipv4_cidr_block
  }

  master_authorized_networks_config {
    dynamic "cidr_blocks" {
      for_each = var.gke_authorized_networks
      content {
        cidr_block   = cidr_blocks.value["cidr_block"]
        display_name = cidr_blocks.value["display_name"]
      }
    }
  }

  network    = google_compute_network.gke.id
  subnetwork = google_compute_subnetwork.gke.id

  ip_allocation_policy {
    services_secondary_range_name = google_compute_subnetwork.gke.secondary_ip_range[index(google_compute_subnetwork.gke.secondary_ip_range.*.range_name, var.gke_services.range_name)].range_name
    cluster_secondary_range_name  = google_compute_subnetwork.gke.secondary_ip_range[index(google_compute_subnetwork.gke.secondary_ip_range.*.range_name, var.gke_default_pods.range_name)].range_name
  }

  release_channel {
    channel = var.gke_release_channel
  }

  maintenance_policy {
    recurring_window {
      start_time = var.gke_maintenance_policy.start_time
      end_time   = var.gke_maintenance_policy.end_time
      recurrence = var.gke_maintenance_policy.recurrence
    }
  }

  vertical_pod_autoscaling {
    enabled = var.gke_vertical_pod_autoscaling_enabled
  }

  dynamic "authenticator_groups_config" {
    for_each = var.gke_security_group_rbac_enabled ? [1] : []
    content {
      security_group = "gke-security-groups@${var.gke_security_group_domain}"
    }
  }

  dynamic "workload_identity_config" {
    for_each = var.gke_workload_identity_enabled ? [1] : []
    content {
      workload_pool = "${data.google_project.main.project_id}.svc.id.goog"
    }
  }

  logging_config {
    enable_components = ["SYSTEM_COMPONENTS", "WORKLOADS"]
  }
  monitoring_config {
    enable_components = ["SYSTEM_COMPONENTS"]
  }

  addons_config {
    dns_cache_config {
      enabled = true
    }
    gce_persistent_disk_csi_driver_config {
      enabled = true
    }
  }

  depends_on = [google_compute_router_nat.gke, google_compute_subnetwork.gke, google_project_iam_member.gke_shared_network_roles_agent_user, google_project_iam_member.gke_shared_network_roles_fw_admin]
}

resource "google_container_node_pool" "default" {
  project  = var.gcp_project_id
  name     = var.gke_default_node_pool_name
  location = var.gke_location
  cluster  = google_container_cluster.main.name

  initial_node_count = var.gke_default_node_pool_initial_node_count

  autoscaling {
    min_node_count = var.gke_default_node_pool_min_node_count
    max_node_count = var.gke_default_node_pool_max_node_count
  }

  node_config {
    service_account = google_service_account.gke.email
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]

    dynamic "workload_metadata_config" {
      for_each = var.gke_workload_identity_enabled ? [1] : []
      content {
        mode = "GKE_METADATA"
      }
    }

    labels          = var.gke_default_node_pool_labels
    resource_labels = var.gke_default_node_pool_labels

    machine_type = var.gke_default_node_pool_machine_type
    image_type   = var.gke_default_node_pool_image_type
    disk_type    = var.gke_default_node_pool_disk_type
    disk_size_gb = var.gke_default_node_pool_disk_size_gb
    preemptible  = var.gke_default_node_pool_preemptible
    spot         = var.gke_default_node_pool_spot
    tags         = [local.default_nodes_network_tag, var.gke_default_node_pool_name]
    metadata = {
      disable-legacy-endpoints = "true"
    }
  }

  network_config {
    create_pod_range     = false
    pod_range            = google_compute_subnetwork.gke.secondary_ip_range[index(google_compute_subnetwork.gke.secondary_ip_range.*.range_name, var.gke_default_pods.range_name)].range_name
    enable_private_nodes = true
  }

  management {
    auto_repair  = true
    auto_upgrade = true
  }

  timeouts {
    create = "30m"
    update = "30m"
    delete = "30m"
  }

  depends_on = [google_compute_subnetwork.gke]
}

resource "cloudflare_record" "gke" {
  count   = var.cf_gke_dns_record_enabled ? 1 : 0
  zone_id = data.cloudflare_zone.main.zone_id
  name    = module.name.domain_friendly
  value   = google_container_cluster.main.endpoint
  type    = "A"
  ttl     = "300"
  proxied = false
}
