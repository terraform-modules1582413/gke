output "cluster_basic_info" {
  value = "GKE cluster ${google_container_cluster.main.name} FDQN is ${cloudflare_record.gke[0].name}.${var.cf_base_domain} [ ${google_container_cluster.main.endpoint} ]. Egress connections are made from ${cloudflare_record.gke_outside_dns[0].name}.${var.cf_base_domain} [ ${google_compute_address.gke_outside_public_ip.address} ]"
}
output "cluster_name" {
  value = google_container_cluster.main.name
}
output "cluster_ca_cert" {
  value = google_container_cluster.main.master_auth[0].cluster_ca_certificate
}
output "cluster_endpoint" {
  value = google_container_cluster.main.endpoint
}
output "cluster_full_endpoint" {
  value = "https://${google_container_cluster.main.endpoint}"
}
output "cluster_ipv4_cidr" {
  value = google_container_cluster.main.cluster_ipv4_cidr
}
output "network_name" {
  value = google_compute_network.gke.name
}
output "network_id" {
  value = google_compute_network.gke.id
}
output "network_self_link" {
  value = google_compute_network.gke.self_link
}
output "network_project_id" {
  value = google_compute_network.gke.project
}
output "global_network_tag" {
  value = local.default_nodes_network_tag
}
output "sa_email" {
  value = google_service_account.gke.email
}
output "pods_range_name" {
  value = var.gke_custom_pods_enabled ? var.gke_custom_pods.range_name : var.gke_default_pods.range_name
}
output "pods_ip_ranges" {
  value = flatten([
    [var.gke_default_pods.ip_range],
    var.gke_custom_pods_enabled ? [var.gke_custom_pods.ip_range] : []
  ])
}
output "outside_public_ip" {
  value = google_compute_address.gke_outside_public_ip.address
}
