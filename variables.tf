variable "project_prefix" {
  type = string
}
variable "project_environment" {
  type = string
}
variable "project_name" {
  type = string
}

variable "gcp_project_id" {
  type = string
}
variable "gcp_region" {
  type = string
}
variable "gcp_shared_networks_enabled" {
  type    = bool
  default = false
}
variable "gcp_shared_networks_host_project_id" {
  type     = string
  nullable = true
  default  = ""
}
variable "gke_private_vpc_connection_enabled" {
  type    = bool
  default = true
}
variable "gke_location" {
  type = string
}
variable "gke_description" {
  type = string
}
variable "gke_release_channel" {
  type    = string
  default = "STABLE"
}
variable "gke_master_ipv4_cidr_block" {
  type    = string
  default = "172.16.0.0/28"
}
variable "gke_authorized_networks" {
  type = list(object({
    cidr_block   = string
    display_name = string
  }))
}
variable "gke_maintenance_policy" {
  type = object({
    start_time = string
    end_time   = string
    recurrence = string
  })
  default = {
    start_time = "2022-05-01T00:00:00Z"
    end_time   = "2022-05-01T04:00:00Z"
    recurrence = "FREQ=WEEKLY;BYDAY=TU,WE,TH;INTERVAL=1"
  }
}
variable "gke_nodes_ip_range" {
  type    = string
  default = "10.10.0.0/16"
}
variable "gke_services" {
  type = object({
    range_name = string
    ip_range   = string
  })
  default = {
    range_name = "services"
    ip_range   = "10.20.0.0/16"
  }
}
variable "gke_default_pods" {
  type = object({
    range_name = string
    ip_range   = string
  })
  default = {
    range_name = "pods"
    ip_range   = "10.30.0.0/16"
  }
}
variable "gke_custom_pods_enabled" {
  type    = bool
  default = false
}
variable "gke_custom_pods" {
  type = object({
    range_name = string
    ip_range   = string
  })
  default = {
    range_name = "custom-pods"
    ip_range   = "10.40.0.0/16"
  }
}
variable "gke_default_node_pool_name" {
  type    = string
  default = "default"
}
variable "gke_default_node_pool_initial_node_count" {
  type    = number
  default = 1
}
variable "gke_default_node_pool_min_node_count" {
  type    = number
  default = 1
}
variable "gke_default_node_pool_max_node_count" {
  type    = number
  default = 1
}
variable "gke_default_node_pool_labels" {
  type = map(string)
  default = {
    "name" = "default"
  }
}
variable "gke_default_node_pool_machine_type" {
  type    = string
  default = "e2-small"
}
variable "gke_default_node_pool_image_type" {
  type    = string
  default = "UBUNTU_CONTAINERD"
}
variable "gke_default_node_pool_disk_type" {
  type    = string
  default = "pd-standard"
}
variable "gke_default_node_pool_disk_size_gb" {
  type    = number
  default = 24
}
variable "gke_default_node_pool_preemptible" {
  type    = bool
  default = false
}
variable "gke_default_node_pool_spot" {
  type    = bool
  default = false
}
variable "gke_security_group_rbac_enabled" {
  type    = bool
  default = false
}
variable "gke_vertical_pod_autoscaling_enabled" {
  type    = bool
  default = false
}
variable "gke_security_group_domain" {
  type    = string
  default = ""
}
variable "gke_workload_identity_enabled" {
  type    = bool
  default = false
}

variable "cf_base_domain" {
  type = string
}
variable "cf_gke_dns_record_enabled" {
  type    = bool
  default = true
}
variable "cf_gke_outside_dns_record_enabled" {
  type    = bool
  default = true
}
