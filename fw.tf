locals {
  all_pods_ranges = flatten([
    [google_compute_subnetwork.gke.secondary_ip_range[index(google_compute_subnetwork.gke.secondary_ip_range.*.range_name, var.gke_default_pods.range_name)].ip_cidr_range],
    var.gke_custom_pods_enabled ? [google_compute_subnetwork.gke.secondary_ip_range[index(google_compute_subnetwork.gke.secondary_ip_range.*.range_name, var.gke_custom_pods.range_name)].ip_cidr_range] : []
  ])
}

resource "google_compute_firewall" "pods_to_nodes" {
  project = var.gcp_shared_networks_enabled ? var.gcp_shared_networks_host_project_id : var.gcp_project_id
  network = google_compute_network.gke.name
  name    = "${module.name.short}-fw-allow-all-pods-to-all-nodes-${module.name.appendix}"

  allow {
    protocol = "tcp"
    ports    = null
  }
  allow {
    protocol = "udp"
    ports    = null
  }
  allow {
    protocol = "icmp"
    ports    = null
  }
  allow {
    protocol = "esp"
    ports    = null
  }
  allow {
    protocol = "ah"
    ports    = null
  }
  allow {
    protocol = "sctp"
    ports    = null
  }

  source_ranges = local.all_pods_ranges
  target_tags   = [local.default_nodes_network_tag]
  priority      = 10

  log_config {
    metadata = "INCLUDE_ALL_METADATA"
  }
}

resource "google_compute_firewall" "master_to_nodes" {
  project = var.gcp_shared_networks_enabled ? var.gcp_shared_networks_host_project_id : var.gcp_project_id
  network = google_compute_network.gke.name

  name = "${module.name.short}-fw-allow-master-to-all-nodes-${module.name.appendix}"

  allow {
    protocol = "tcp"
    ports    = ["8443", "443", "10250"]
  }

  source_ranges = [var.gke_master_ipv4_cidr_block]
  target_tags   = [local.default_nodes_network_tag]
  priority      = 10

  log_config {
    metadata = "INCLUDE_ALL_METADATA"
  }
}

resource "google_compute_firewall" "pods_to_monitoring_nodes" {
  project = var.gcp_shared_networks_enabled ? var.gcp_shared_networks_host_project_id : var.gcp_project_id
  network = google_compute_network.gke.name

  name = "${module.name.short}-fw-allow-pods-to-all-monitoring-nodes-${module.name.appendix}"

  allow {
    protocol = "tcp"
    ports    = ["10255"]
  }

  source_ranges = local.all_pods_ranges
  source_tags   = [local.default_nodes_network_tag]
  target_tags   = [local.default_nodes_network_tag]
  priority      = 5

  log_config {
    metadata = "INCLUDE_ALL_METADATA"
  }
}

resource "google_compute_firewall" "nodes_to_nodes" {
  project = var.gcp_shared_networks_enabled ? var.gcp_shared_networks_host_project_id : var.gcp_project_id
  network = google_compute_network.gke.name

  name = "${module.name.short}-fw-allow-all-nodes-to-all-nodes-${module.name.appendix}"

  allow {
    protocol = "tcp"
    ports    = ["1-65535"]
  }
  allow {
    protocol = "udp"
    ports    = ["1-65535"]
  }
  allow {
    protocol = "icmp"
    ports    = null
  }
  source_ranges = [var.gke_nodes_ip_range]
  target_tags   = [local.default_nodes_network_tag]
  priority      = 10

  log_config {
    metadata = "INCLUDE_ALL_METADATA"
  }
}

resource "google_compute_firewall" "block_all" {
  project = var.gcp_shared_networks_enabled ? var.gcp_shared_networks_host_project_id : var.gcp_project_id
  network = google_compute_network.gke.name

  name = "${module.name.short}-fw-deny-all-to-all-nodes-${module.name.appendix}"

  deny {
    protocol = "all"
  }

  source_ranges = ["0.0.0.0/0"]
  target_tags   = [local.default_nodes_network_tag]
  priority      = 998

  log_config {
    metadata = "INCLUDE_ALL_METADATA"
  }

}
